#!/bin/bash

# Check dependencies
deps=( 'git' 'curl' 'jq' 'fzf' )
for dep in ${deps[@]}; do
	[ ! $( command -v "$dep" ) ] && echo "Missing dependency: '$dep'. Please install it via your package manager." && exit 1
done

# Store the script path in a variable
DIR=$( cd -- "$( dirname -- "$(readlink "${BASH_SOURCE[0]}")" )" &> /dev/null && pwd )

# Load config
source "${DIR}/../config/newscript.cfg"

# Load external functions
source "${DIR}/add-license.src"
source "${DIR}/add-input-h.src"
source "${DIR}/add-usage.src"
source "${DIR}/add-dir.src"
source "${DIR}/add-submodules.src"

# Variables
projpath="$( pwd )"
target_dir="${scripts_dir}"

# Functions
function usage()
{
	echo "Usage: ${0} [OPTIONS] [-n PROJ_NAME] [-f FILE_NAME]"
	echo "  -n | --name             Project name"
	echo "  -f | --file             Generate a script file in the working directory. Ignores all project related options"
	echo "  -t | --target-dir       Target directory"
	echo "  -a | --all              All options"
	echo "  -l | --license          Add License to the project"
	echo "  -d | --add-dir          Add a DIR variable, containing the script's path"
	echo "  -u | --usage            Add 'usage' function"
	echo "  -i | --input-handler    Add input handler (similar to getopts)"
	echo "  -g | --git-repo         Initialize git repository"
	echo "  -s | --git-submodules   Add git submodules from \$remote_proj"
	echo "  -r | --run-git-commands Run git commands"
	echo "  -h | --help             Display help"
	echo
	echo "More info and examples on https://gitlab.com/darwin.brandao/mkbash"
	exit 1
}

# Input: 
## https://stackoverflow.com/a/63653944
## https://www.gnu.org/software/bash/manual/bash.html#Conditional-Constructs
## Parsing:
##		-n		= 1 flag
##		-ldug	= 4 flags
##		--name	= 1 flag
while [ $# -gt 0 ]; do
	
	# Method:
	#	Remove first char from input
	#	Loop all chars from result (input without first char)
	#	If result's first char is "-", break out of the loop after running once
	#	Else, loop all chars until the end, checking for matches

	if [ "${1:0:1}" == "-" ]; then
	
		# Remove first char from $1
		result="${1:1}"
	
		# Loop all chars from $result
		for ((i=0; i < ${#result}; i++)); do
	
			# Check if result's first char is "-" (--name would become -name)
			#	True: $arg stores $1 (example: --name)
			#	False: $arg stores the current char from $result in the loop (example: the letter "l", from the "-ldug" input)
			arg="$( [ "${result:0:1}" == "-" ] && echo $1 || echo ${result:$i:1} )"
			
			# Check value
			case "${arg}" in
				n | --name)
					name="$2"
				;;
				f | --file)
					gen_file=true
					filename="${2}"
					filepath="${projpath}/${filename}"
				;;
				t | --target-dir)
					target_dir="$2"
					projpath="$target_dir"
				;;
				l | --license | a | --all)
					add_license=true
				;;&
				d | --add-dir | a | --all)
					add_dir=true
				;;&
				u | --usage | a | --all)
					add_usage=true
				;;&
				i | --input-handler | a | --all)
					add_input=true
				;;&
				g | --git-repo | a | --all)
					add_git=true
				;;&
				s | --git-submodules | a | --all)
					add_submodules=true
				;;&
				r | --run-git-commands | a | --all)
					run_git_cmds=true
				;;
				h | --help)
					usage
				;;
				!"$arg")
					echo "Error: Unknown option \"$arg\""
					usage
				;;
			esac
	
			# Break out of the loop, if $result's first char is "-"
			[ "${result:0:1}" == "-" ] && break
		done
		shift
	else
		shift
	fi
done

# Check if project name was given, then set $projpath and $filepath
if [ -n "$name" ]; then
	projpath="${target_dir}/${name}"
	filepath="${projpath}/${name}.sh"
else
	filepath="${projpath}/${filename}.sh"
fi

# Input checks
if [ "$gen_file" ]; then
	[ -z "$filename" ] && echo "Error: provide a name for the script file." && usage
else
	[ -z "$name" ] && echo "Error: Provide a name for the project." && usage
fi

if [ ! "$gen_file" ]; then

	# Make project directory
	if [ ! -d "$projpath" ]; then 
		echo "[?] Creating directory'$projpath'"
		mkdir -p "$projpath"
	fi
	
	# Add MIT License
	if [ "$add_license" ]; then
		echo "[?] Adding license file to '$projpath'"
		add-license "$projpath"
	fi

fi

# Add Shebang
echo '#!/bin/bash' > "$filepath"

# Make script executable
echo "[?] Making script executable"
chmod +x "$filepath"

# Add DIR variable
if [ "$add_dir" ]; then
	echo "[?] Adding DIR variable to '$filepath'"
	add-dir "$filepath"
fi

# Add Usage function
if [ "$add_usage" ]; then
	echo "[?] Adding usage function to '$filepath'"
	add-usage "$filepath"
fi

# Add Input Handler
if [ "$add_input" ]; then
	echo "[?] Adding input handler to '$filepath'"
	add-input-h "$filepath"
fi

if [ ! "$gen_file" ]; then

	# Initialize git repository
	if [ "$add_git" ]; then 
		echo "[?] Initializing git repo in '$projpath'"
		git --git-dir "${projpath}/.git" init
	fi
	
	# Add git submodules
	if [ "$add_submodules" ]; then
		add-submodules "$projpath"
	fi
	
	# Run git commands
	if [ "$run_git_cmds" ]; then
		echo "[?] Running git commands in '$projpath'"
		git -C "${projpath}" add "$projpath/*"
		git -C "${projpath}" commit -m "First commit"
		git -C "${projpath}" remote add origin "${remote_repo}/${name}"
		git -C "${projpath}" push origin master
	
		echo "[?] Checking out to dev branch"
		git -C "${projpath}" branch dev
		git -C "${projpath}" checkout dev
	fi
fi
