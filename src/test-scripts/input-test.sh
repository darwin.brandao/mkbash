#!/bin/bash

# Input: 
## https://stackoverflow.com/a/63653944
## https://www.gnu.org/software/bash/manual/bash.html#Conditional-Constructs
## Parsing:
##      -n      = 1 flag
##      -ldug   = 4 flags
##      --name  = 1 flag
while [ $# -gt 0 ]; do
    
    # Method:
    #   Remove first char from input
    #   Loop all chars from result (input without first char)
    #   If result's first char is "-", break out of the loop after running once
    #   Else, loop all chars until the end, checking for matches
 
	if [ "${1:0:1}" == "-" ]; then
	    # Remove first char from $1
	    result="${1:1}"
	
	    # Loop all chars from $result
	    for ((i=0; i < ${#result}; i++)); do
	 
	        # Check if result's first char is "-" (--name would become -name)
	        #   True: $arg stores $1 (example: --name)
	        #   False: $arg stores the current char from $result in the loop (example: the letter "l", from the "-ldug" input)
	        #arg="$( [ "${result:0:1}" == "-" ] && echo $1 || echo ${result:$i:$(($i+1))} )"
	        arg="$( [ "${result:0:1}" == "-" ] && echo $1 || echo ${result:$i:1} )"
	
	        # Check value
	        case "${arg}" in
	            a | --opt-a)
	                value_a="$2"
					echo "a: $2"
	            ;;
	            b | --opt-b)
	                value_b="$2"
					echo "b: $2"
				;;
				h | --help)
	                usage
	            ;;
				*)
					echo "Error: Unknown option \"$arg\""
					usage
				;;
	        esac
	
	        # Break out of the loop, if $result's first char is "-"
	        [ "${result:0:1}" == "-" ] && break
	    done
	    shift
	else
		shift
	fi
done
