#!/bin/bash

source "../../config/newscript.cfg"
source ../add-submodules.src

mkdir -p "test"
git -C "test" init
add-submodules "./test"
