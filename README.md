# **mkbash**

I wrote this script to help me create other scripts and projects. I usually add the same functionalities and run the same commands on most of my bash scripts, so I decided to write  **mkbash**  to help me with this.

## Dependencies

You need to install **git**, **curl**, **jq** and **fzf**.

- ```
  Fedora based systems:
  sudo dnf install git curl jq fzf
  
  Debian/Ubuntu based systems:
  sudo apt install git curl jq fzf
  
  Arch based systems:
  sudo pacman -S git curl jq fzf
  ```
  
  **fzf** is used to display a menu in the terminal, when adding submodules with  **mkbash** .

## Config

**You must configure before using it!**

Otherwise, my name, GitLab account and etc will be all over your project (LICENSE file, git remote origin repo, etc).

To configure  **mkbash**  , you must change the variables in 'config/newscript.cfg' file.

Your 'config/newscript.cfg' should look like this:

```
author="YOUR NAME HERE"
scripts_dir="/home/your_user/your/projects/directory"
remote_repo="https://gitlab.com/YOUR_NICKNAME_HERE"
remote_proj="https://gitlab.com/api/v4/users/YOUR_NICKNAME_HERE/projects"
```

## Usage

```
Usage: mkbash [OPTIONS] [-n PROJ_NAME] [-f FILE_NAME]
  -n | --name             Project name
  -f | --file             Generate a script file in the working directory. Ignores all project related options
  -t | --target-dir       Target directory
  -a | --all              All options
  -l | --license          Add License to the project
  -d | --add-dir          Add a DIR variable, containing the script's path
  -u | --usage            Add 'usage' function
  -i | --input-handler    Add input handler (similar to getopts)
  -g | --git-repo         Initialize git repository
  -s | --git-submodules   Add git submodules from $remote_proj
  -r | --run-git-commands Run git commands
  -h | --help             Display help
```

By default,  **mkbash**  makes a directory for the project and a script, both with the same name (project's name).

## Examples:

## Create a project, without any additional options

**Command:**

```
mkbash -n example
```

**Result:**

```
example/
└── example.sh
```

**example.sh:**

```
#!/bin/bash
```

Other ways of writing this command:

```
mkbash --name example
```

## Create a script file in the current working directory

Creates a script file with shebang and execution permission.

**Command:**

```
mkbash -f script.sh
```

**Result:**

```
script.sh    .rwxr-xr-x. darwin darwin  12 B  Wed Mar 22 22:00:06 2023
```

**script.sh:**

```
#!/bin/bash
```

## Set the target directory

Specify the directory to create the project into.

**Command:**

```
mkbash -n my_project -t /home/path/to/directory
```

Other ways of writing this command:
```
mkbash -f my_file.txt -t /home/path/to/directory
mkbash -n my_project --target-dir /home/path/to/directory
mkbash -f my_file.txt --target-dir /home/path/to/directory
```

## Create project with ALL the additional options

**Command:**

```
mkbash -a -n example
```

This command creates a project with all the additional options.

It's equivalent to adding all the following options:

```
  -l | --license          Add License to the project
  -d | --add-dir          Add a DIR variable, containing the script's path
  -u | --usage            Add 'usage' function
  -i | --input-handler    Add input handler (similar to getopts)
  -g | --git-repo         Initialize git repository
  -s | --git-submodules   Add git submodules from $remote_proj
  -r | --run-git-commands Run git commands
```

The '-s' option fetches your public personal projects from GitLab, and displays them in a multi selection menu ('fzf -m'), so you can select the projects you want to use as submodules by selecting them and pressing TAB. When finished selecting them, press ENTER to go to the next step.

The '-r' option runs the git commands I use the most in every bash project. They are:

```
git add *
git commit -m "First commit"
git remote add origin "${remote_repo}/${name}"
git push origin master
git branch dev
git checkout dev
```

Of course, it will ask you to log in to your GitLab account, so it can push the project to a new PRIVATE repository in GitLab. Just hit CTRL+C to skip it, if you want to.

**Result:**

```
example/
├── example.sh
├── .git
│   └── (ommited files within .git for readabillity purposes)
├── LICENSE
└── submodules
    ├── your-submodule-1
    │   └── (your submodule files)
    └── your-submodule-2
        └── (your submodule files)
```

**example.sh:**

```
#!/bin/bash

# Store the script path in a variable
DIR=$( cd -- "$( dirname "$(readlink -f "$0")" )" &> /dev/null && pwd )

function usage()
{
    echo "Usage: ${0} [OPTIONS] [-b VALUE]"
    echo "  -a      Option a"
    echo "  -b      Argument b"
    echo "  -h      Display help"
    exit 1
}

# Input: 
while [ $# -gt 0 ]; do

    if [ "${1:0:1}" == "-" ]; then
        # Remove first char from $1
        result="${1:1}"

        # Loop all chars from $result
        for ((i=0; i < ${#result}; i++)); do

            # Check if result's first char is "-" (--name would become -name)
            arg="$( [ "${result:0:1}" == "-" ] && echo $1 || echo ${result:$i:1} )"

            # Check value
            case "${arg}" in
                b | --opt-b | a)
                    value_b="$2"
                    echo "b: $2"
                ;;&
                c | --opt-c | a)
                    value_c="$2"
                    echo "c: $2"
                ;;&
                d | --opt-d | a)
                    value_d="$2"
                    echo "d: $2"
                ;;&
                h | --help)
                    usage
                ;;
                !"arg")
                    echo "Error: Unknown option \"$arg\""
                    usage
                ;;
            esac

            # Break out of the loop, if $result's first char is "-"
            [ "${result:0:1}" == "-" ] && break
        done
        shift
    else
        shift
    fi
done
```

Other ways of writing this command:

```
mkbash --all -n example
mkbash -lduigsr -n example
mkbash -lduigsr --name example
mkbash -lduigsrn example
```

## Create project with license already included in the directory

**Command:**

```
mkbash -l -n example
```

**Result:**

```
example/
├── example.sh
└── LICENSE
```

Other ways of writing this command:

```
mkbash --license -n example
mkbash --license --name example
mkbash -l --name example
mkbash -ln example
```

## Create project with the $DIR variable within the script

This variable stores the directory where the script file is in.

Not the working directory, not the directory the user is calling the script from, it stores the directory where the script file is in.

It's useful, because you can easily access other files within the script's directory, such as config files and other scripts.

Example: "${DIR}/config/newscript.cfg" will always return the absolute path of the config file, even when you run the script from a symbolic link in '\~/.local/bin/' directory, such as '\~/.local/bin/mkbash'.

**Command:**

```
mkbash -d -n example
```

**Result:**

```
example/
└── example.sh
```

**example.sh:**

```
#!/bin/bash

# Store the script path in a variable
DIR=$( cd -- "$( dirname "$(readlink -f "$0")" )" &> /dev/null && pwd )
```

Other ways of writing this command:

```
mkbash --add-dir -n example
mkbash --add-dir --name example
mkbash -d --name example
mkbash -dn example
```

## Create project with the 'usage()' function within the script

**Command:**

```
mkbash -u -n example
```

**Result:**

```
example/
└── example.sh
```

**example.sh:**

```
#!/bin/bash
function usage()
{
    echo "Usage: ${0} [OPTIONS] [-b VALUE]"
    echo "  -a      Option a"
    echo "  -b      Argument b"
    echo "  -h      Display help"
    exit 1
}
```

Other ways of writing this command:

```
mkbash --usage -n example
mkbash --usage --name example
mkbash -u --name example
mkbash -un example
```

## Create project with the 'input handler' function within the script

**Command:**

```
mkbash -i -n example
```

It's similar to getopts , but with some changes. It comes with a basic options structure, in which you can use '-a' as a way to add all options with a single flag.

It's highly recommended to use this option along with the '-u' option, so you have both the 'usage()' function and the 'input handler' in the same file.

**Result:**

```
example/
└── example.sh
```

**example.sh:**

```
#!/bin/bash
# Input: 
while [ $# -gt 0 ]; do

    if [ "${1:0:1}" == "-" ]; then
        # Remove first char from $1
        result="${1:1}"

        # Loop all chars from $result
        for ((i=0; i < ${#result}; i++)); do

            # Check if result's first char is "-" (--name would become -name)
            arg="$( [ "${result:0:1}" == "-" ] && echo $1 || echo ${result:$i:1} )"

            # Check value
            case "${arg}" in
                b | --opt-b | a)
                    value_b="$2"
                    echo "b: $2"
                ;;&
                c | --opt-c | a)
                    value_c="$2"
                    echo "c: $2"
                ;;&
                d | --opt-d | a)
                    value_d="$2"
                    echo "d: $2"
                ;;&
                h | --help)
                    usage
                ;;
                !"arg")
                    echo "Error: Unknown option \"$arg\""
                    usage
                ;;
            esac

            # Break out of the loop, if $result's first char is "-"
            [ "${result:0:1}" == "-" ] && break
        done
        shift
    else
        shift
    fi
done
```

Other ways of writing this command:

```
mkbash --input-handler -n example
mkbash --input-handler --name example
mkbash --input-handler --usage -n example
mkbash --input-handler -u -n example
mkbash -i --name example
mkbash -in example
```

## Create project and initialize git repository

**Command:**

```
mkbash -g -n example
```

**Result:**

```
example/
├── example.sh
└── .git
│   └── (ommited files within .git for readabillity purposes)
```

**example.sh:**

```
#!/bin/bash
```

Other ways of writing this command:

```
mkbash --git-repo -n example
mkbash --git-repo --name example
mkbash -g --name example
mkbash -gn example
```

## Create project and add submodules from your GitLab projects

**Command:**

```
mkbash -g -s -n example
```

Of course, this command only works if you have a git repository in the project's directory. So, it's highly recommended to run it along with the '-g' flag.

**Result:**

```
example/
├── example.sh
├── .git
│   └── (ommited files within .git for readabillity purposes)
├── .gitmodules
└── submodules
    ├── your-submodule-1
    │   └── (your submodule files)
    └── your-submodule-2
        └── (your submodule files)
```

**example.sh:**

```
#!/bin/bash
```

Other ways of writing this command:

```
mkbash --git-repo --add-submodules -n example
mkbash --git-repo -s -n example
mkbash -g --add-submodules -n example
mkbash -gs -n example
mkbash -gsn example
```

## Create project and run git commands

**Command:**

```
mkbash -g -r -n example
```

Of course, this command only works if you have a git repository in the project's directory. So, it's highly recommended to run it along with the '-g' flag.

The '-r' option runs the git commands I use the most in every bash project. They are:

```
git add *
git commit -m "First commit"
git remote add origin "${remote_repo}/${name}"
git push origin master
git branch dev
git checkout dev
```

**Result:**

```
example/
├── example.sh
├── .git
    └── (ommited files within .git for readabillity purposes)
```

**example.sh:**

```
#!/bin/bash
```

Other ways of writing this command:

```
mkbash --git-repo --run-git-commands -n example
mkbash --git-repo -r -n example
mkbash -g --run-git-commands -n example
mkbash -gr -n example
mkbash -grn example
```
